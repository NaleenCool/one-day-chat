# One Day Chat

This is a simple chat app.

<img src="screenshots.png" />


### Features
* Simple chat function.

## Getting Started

### Wireframe
You can see it from here. 
            https://angular-test-backend-yc4c5cvnnq-an.a.run.app/template.html
    
### graphql APIs
You can see it from here. 
            https://angular-test-backend-yc4c5cvnnq-an.a.run.app/graphiql

### Install and run on android
        cd one-day-chat
        npm install
        react-native run-android or npx react-native run-ios


## Application Main Modules

### Project Structure

## Potential improvements


* Changes are required for  ui


## Known issues

* Reopen the page, the text editor should keep their text. 
* Front-end should handle the Unsent messages

## Suggestions
* Need more time to implement missing features


