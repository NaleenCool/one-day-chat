import React, {
  useState,
  useCallback,
  useEffect,
  useContext,
  useRef,
} from 'react';
import {
  Text,
  View,
  SafeAreaView,
  StatusBar,
  TouchableHighlight,
} from 'react-native';
import {Button} from 'react-native-paper';
import {GiftedChat} from 'react-native-gifted-chat';
import {gql, useQuery, useLazyQuery, useMutation, from} from '@apollo/client';
import {MainContext} from '../../../App';
import ChannelId from '../../constants/ChannelId';
import {
  MESSAGES_FETCH_LATEST,
  MESSAGES_FETCH_MORE_PREVIOUS,
  MESSAGES_FETCH_MORE_LATEST,
  MESSAGE_POST,
} from '../../service/query';
import {storeMessageData, getMessageData} from '../../util/util';

import styles from './styles';
import UserId from '../../constants/UserId';

export default ({navigation}) => {
  const context = useContext(MainContext);
  const [channelId, setChannelId] = React.useState(context.channelId);
  const [userId, setUserId] = React.useState(context.userId);
  const [currentMessage, setCurrentMessage] = React.useState('');
  const [messageList, setMessageList] = useState([]);
  const mountedRef = useRef();

  useEffect(() => {
    if (userId != context.userId) {
      loadAllMessageList();
    }
    setUserId(context.userId);
  }, [context.userId]);

  useEffect(() => {
    if (channelId != context.channelId) {
      loadAllMessageList();
    }
    setChannelId(context.channelId);
  }, [context.channelId]);

  useEffect(() => {
    loadAllMessageList();
  }, []);

  const onSend = useCallback((messages = []) => {
    setMessages(previousMessages =>
      GiftedChat.append(previousMessages, messages),
    );
  }, []);

  const [fetchMoreLatestMessages, latestMessagesData] = useLazyQuery(
    MESSAGES_FETCH_MORE_LATEST,
  );

  const [fetchMorePreviousMessages, previousMessagesData] = useLazyQuery(
    MESSAGES_FETCH_MORE_PREVIOUS,
  );

  const [fetchAllMessages, messageData] = useLazyQuery(MESSAGES_FETCH_LATEST, {
    variables: {channelId: context.channelId},
  });

  const [postMessages, postMessageData] = useMutation(MESSAGE_POST);

  const renderReadMoreLatestMessages = () => {
    return (
      <View
        style={{
          height: 50,
          width: 150,
          marginLeft: 10,
          alignSelf: 'flex-start',
        }}>
        <Button
          icon="download"
          mode="contained"
          onPress={() => {
            loadMoreLatestMessages();
          }}>
          Read more
        </Button>
      </View>
    );
  };

  const renderReadMorePreviousMessages = () => {
    return (
      <View
        style={{
          height: 50,
          width: 150,
          marginLeft: 10,
          alignSelf: 'flex-start',
        }}>
        <Button
          icon="upload"
          mode="contained"
          onPress={() => {
            loadMorePreviousMessages();
          }}>
          Read more
        </Button>
      </View>
    );
  };

  const loadMoreLatestMessages = () => {
    if (messageList.length > 0) {
      fetchMoreLatestMessages({
        variables: {
          channelId: context.channelId,
          messageId: messageList[0]._id,
          old: false,
        },
      })
        .then(data => {
          validateLatestMessages(data);
        })
        .catch(error => {
          console.log('random error happened');
        });
    }
  };

  const loadMorePreviousMessages = () => {
    if (messageList.length > 0) {
      fetchMorePreviousMessages({
        variables: {
          channelId: context.channelId,
          messageId: messageList[messageList.length - 1]._id,
          old: true,
        },
      })
        .then(data => {
          validatePreviousMessages(data);
        })
        .catch(error => {
          console.log('random error happened');
        });
    }
  };

  const loadAllMessageList = () => {
    fetchAllMessages({
      variables: {channelId: context.channelId},
    }).then(data => {
      validateAllMessageList(data);
    });
  };

  validateAllMessageList = data => {
    if (data && data.error) {
      console.log('error', data.error);
    } else if (data && data.data && data.data.fetchLatestMessages) {
      setMessageList(loadMessageDataToTheList(data.data.fetchLatestMessages));
    }
  };

  validatePreviousMessages = data => {
    if (data && data.error) {
      console.log('error', data.error);
    } else if (data && data.data && data.data.fetchMoreMessages) {
      let newList = loadMessageDataToTheList(data.data.fetchMoreMessages);
      setMessageList(messageList.concat(newList));
    }
  };

  validateLatestMessages = data => {
    if (data && data.error) {
      console.log('error', data.error);
    } else if (data && data.data && data.data.fetchMoreMessages) {
      let newList = loadMessageDataToTheList(data.data.fetchMoreMessages);
      setMessageList(newList.concat(messageList));
    }
  };

  validatePostMessage = data => {
    if (data && data.error) {
      console.log('error', data.error);
    } else if (data && data.data && data.data.postMessage) {
      let newList = [...messageList];
      let newPost = addNewMessageToTheList(data.data.postMessage);
      newList.unshift(newPost);
      setMessageList(newList);
    }
  };

  const sendMessage = message => {
    if (message && message.length > 0 && message[0].text != '') {
      postMessages({
        variables: {
          channelId: context.channelId,
          text: message[0].text,
          userId: context.userId,
        },
      })
        .then(data => {
          validatePostMessage(data);
        })
        .catch(error => {
          console.log('random error happened');
        });
    }
  };

  const addNewMessageToTheList = data => {
    let currentUserObj = {
      _id: data.userId == context.userId ? 1 : 2,
      name: data.userId,
      avatar: getUserIcon(data.userId),
    };

    let messageObj = {
      _id: data.messageId,
      text: data.text,
      createdAt: data.datetime,
      user: currentUserObj,
    };
    return messageObj;
  };

  const loadMessageDataToTheList = dataArray => {
    //check the messagearray length
    let array = [];
    let responseUserObj = {};
    let currentUserObj = {};
    let messageObj = {};

    if (typeof dataArray !== 'undefined' && dataArray.length > 0) {
      var arrayLength = dataArray.length;
      for (var i = 0; i < arrayLength; i++) {
        if (dataArray[i].userId == context.userId) {
          currentUserObj = {
            _id: 1,
            name: dataArray[i].userId,
            avatar: getUserIcon(dataArray[i].userId),
          };
          messageObj = {
            _id: dataArray[i].messageId,
            text: dataArray[i].text,
            createdAt: dataArray[i].datetime,
            user: currentUserObj,
          };
        } else {
          responseUserObj = {
            _id: 2,
            name: dataArray[i].userId,
            avatar: getUserIcon(dataArray[i].userId),
          };
          messageObj = {
            _id: dataArray[i].messageId,
            text: dataArray[i].text,
            createdAt: dataArray[i].datetime,
            user: responseUserObj,
          };
        }
        array.push(messageObj);
      }
    }
    return array;
  };

  const getUserIcon = id => {
    if (id == UserId[1]) {
      return require('../../assets/user1.png');
    } else if (id == UserId[2]) {
      return require('../../assets/user2.png');
    } else if (id == UserId[3]) {
      return require('../../assets/user3.png');
    }
    return require('../../assets/user1.png');
  };

  return (
    <SafeAreaView>
      <StatusBar backgroundColor="#6200ee" />
      <View style={{width: '100%', height: '100%'}}>
        <View style={{height: 40}}>
          <Text style={{padding: 10, fontWeight: 'bold'}}>
            {ChannelId[parseInt(context.channelId)]}
          </Text>
        </View>
        {renderReadMorePreviousMessages()}
        <GiftedChat
          messages={messageList}
          showUserAvatar={true}
          renderUsernameOnMessage={true}
          renderUsernameOnMessage={true}
          // onSend={messages => onSend(messages)}
          onSend={sendMessage}
          text={currentMessage}
          onInputTextChanged={message => {
            setCurrentMessage(message);
            //   storeMessageData(message);
          }}
          renderChatFooter={renderReadMoreLatestMessages}
          user={{
            _id: 1,
          }}></GiftedChat>
      </View>
    </SafeAreaView>
  );
};
