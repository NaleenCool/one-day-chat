const ChannelId = {
  1: 'General Channel',
  2: 'Technology Channel',
  3: 'LGTM Channel',
};

export default ChannelId;
