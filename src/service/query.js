import {gql} from '@apollo/client';

export const MESSAGES_FETCH_MORE_LATEST = gql`
  query MessagesFetchMore(
    $channelId: String!
    $messageId: String!
    $old: Boolean!
  ) {
    fetchMoreMessages(channelId: $channelId, messageId: $messageId, old: $old) {
      messageId
      text
      datetime
      userId
    }
  }
`;

export const MESSAGES_FETCH_MORE_PREVIOUS = gql`
  query MessagesFetchMore(
    $channelId: String!
    $messageId: String!
    $old: Boolean!
  ) {
    fetchMoreMessages(channelId: $channelId, messageId: $messageId, old: $old) {
      messageId
      text
      datetime
      userId
    }
  }
`;

export const MESSAGES_FETCH_LATEST = gql`
  query MessagesFetchLatest($channelId: String!) {
    fetchLatestMessages(channelId: $channelId) {
      messageId
      text
      datetime
      userId
    }
  }
`;

export const MESSAGE_POST = gql`
  mutation MessagePost($channelId: String!, $text: String!, $userId: String!) {
    postMessage(channelId: $channelId, text: $text, userId: $userId) {
      messageId
      text
      datetime
      userId
    }
  }
`;
