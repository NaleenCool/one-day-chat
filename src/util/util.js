import AsyncStorage from '@react-native-async-storage/async-storage';

export const storeMessageData = async value => {
  try {
    await AsyncStorage.setItem('@message_Key', value);
  } catch (e) {
    // saving error
  }
};

export const getMessageData = async () => {
  try {
    const value = await AsyncStorage.getItem('@message_Key');
    if (value !== null) {
      return value;
    }
  } catch (e) {
    // error reading value
  }
};
