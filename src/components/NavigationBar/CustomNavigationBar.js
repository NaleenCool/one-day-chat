import React, {useState, useContext, useEffect} from 'react';
import {View} from 'react-native';
import {Menu, Appbar} from 'react-native-paper';
import {MainContext} from '../../../App';
import ChannelId from '../../constants/ChannelId';
import UserId from '../../constants/UserId';

export default CustomNavigationBar = ({navigation}) => {
  const context = useContext(MainContext);
  const [channelId, setChannelId] = React.useState(context.channelId);
  const [userId, setUserId] = React.useState(context.userId);
  const [visibleUserIdMenu, setVisibleUserIdMenu] = React.useState(false);
  const [visibleChannelIdMenu, setVisibleChannelIdMenu] = React.useState(false);
  const openUserIdMenu = () => setVisibleUserIdMenu(true);
  const closeUserIdMenu = () => setVisibleUserIdMenu(false);
  const openChannelIdMenu = () => setVisibleChannelIdMenu(true);
  const closeChannelIdMenu = () => setVisibleChannelIdMenu(false);

  useEffect(() => {
    if (channelId != context.channelId) {
      context.setChannelId(channelId);
    }
  }, [channelId]);

  useEffect(() => {
    if (userId != context.userId) {
      context.setUserId(userId);
    }
  }, [userId]);

  const renderChannelIds = () => {
    return Object.keys(ChannelId).map(function (key) {
      return (
        <Menu.Item
          key={key}
          onPress={() => {
            setChannelId(key); //set channel id
            closeChannelIdMenu();
          }}
          title={ChannelId[key]}
          titleStyle={{color: channelId == key ? '#1932D7' : '#000106'}}
        />
      );
    });
  };

  const renderUserIds = () => {
    return Object.keys(UserId).map(function (key) {
      return (
        <Menu.Item
          key={key}
          onPress={() => {
            setUserId(UserId[key]); //set user id
            closeUserIdMenu();
          }}
          title={UserId[key]}
          titleStyle={{
            color: userId == UserId[key] ? '#1932D7' : '#000106',
          }}
        />
      );
    });
  };

  return (
    <Appbar.Header>
      <Appbar.Content title="One Day Chat App" />
      <Menu
        visible={visibleChannelIdMenu}
        onDismiss={closeChannelIdMenu}
        anchor={
          <Appbar.Action
            icon="television"
            color="white"
            onPress={openChannelIdMenu}
          />
        }>
        {renderChannelIds()}
      </Menu>
      <Menu
        visible={visibleUserIdMenu}
        onDismiss={closeUserIdMenu}
        anchor={
          <Appbar.Action
            icon="account-multiple"
            color="white"
            onPress={openUserIdMenu}
          />
        }>
        {renderUserIds()}
      </Menu>
    </Appbar.Header>
  );
};
