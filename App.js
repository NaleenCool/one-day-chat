/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useState} from 'react';
import 'react-native-gesture-handler';
import {View} from 'react-native';

import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {Provider as PaperProvider} from 'react-native-paper';
import {ApolloProvider} from '@apollo/client';
import CustomNavigationBar from './src/components/NavigationBar/CustomNavigationBar';
import client from './src/service/client';
import ChatScreen from './src/screens/Chat';

export const MainContext = React.createContext(null);

const Stack = createStackNavigator();

function StackMain() {
  const mainContextInitialState = {
    channelId: '1',
    userId: 'Sam',
    message: '',
  };

  const [chatProfile, setChatProfile] = useState(mainContextInitialState);

  function setChannelId(channelId) {
    const newState = {...chatProfile, channelId};
    setChatProfile(newState);
  }

  function setUserId(userId) {
    const newState = {...chatProfile, userId};
    setChatProfile(newState);
  }

  function setMessage(message) {
    const newState = {...chatProfile, message};
    setChatProfile(newState);
  }

  const mainContextSetters = {
    setChannelId,
    setUserId,
    setMessage,
  };

  return (
    <MainContext.Provider value={{...chatProfile, ...mainContextSetters}}>
      <Stack.Navigator
        initialRouteName="Chat Screen"
        screenOptions={{
          header: props => <CustomNavigationBar {...props} />,
        }}>
        <Stack.Screen name="Chat Screen" component={ChatScreen} />
      </Stack.Navigator>
    </MainContext.Provider>
  );
}

const App = () => {
  return (
    <ApolloProvider client={client}>
      <PaperProvider>
        <NavigationContainer>
          <StackMain />
        </NavigationContainer>
      </PaperProvider>
    </ApolloProvider>
  );
};

export default App;
